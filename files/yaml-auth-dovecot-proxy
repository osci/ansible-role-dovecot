#!/usr/bin/env perl
#
# This program was inpired by the example at:
#   http://wiki.dovecot.org/AuthDatabase/Dict
# and was modified to handle YAML-based account files
# and running as non-root.
#

package YAMLAuthProxyDovecot;
use base qw( Net::Server::PreFork );

use strict;
use warnings;
use utf8;

use JSON::XS;
use File::Spec;
use File::stat;
use YAML::XS qw(LoadFile);


YAMLAuthProxyDovecot->run() or die "Could not initialize";


sub default_values
{
  return {
    port              => '/var/run/auth_proxy_dovecot/dovecot-socket/auth.sock|unix',

    log_level         => 2,
    log_file          => 'Sys::Syslog',
    syslog_logsock    => 'unix',
    syslog_ident      => 'auth_proxy_dovecot',
    syslog_facility   => 'daemon',

    background        => 1,
    setsid            => 1,
    pid_file          => '/var/run/auth_proxy_dovecot/auth_proxy_dovecot.pid',

    user              => 'dovecot-proxy',
    group             => 'dovecot-proxy',
    # /!\ this is not a Net::Server parameter
    # it allows to ensure proper UNIX socket permissions without
    # starting this daemon with additional group (and power)
    unix_socket_group      => 'dovecot',

    max_spare_servers => 2,
    min_spare_servers => 1,
    min_servers       => 2,
    max_servers       => 10,
    
    # extra parameters specific to this script
    # (not Net::Server parameters)
    user_data_dir     => '/etc/dovecot/users',
  };
} ## end sub default_values


##################################################


# extra fields may be passed
my $user_struct = {
    uid          => "^([0-9]+|[a-z_][a-z0-9_-]*)\$",  # numeric or name
    gid          => "^([0-9]+|[a-z_][a-z0-9_-]*)\$",  # numeric or name
    home         => '^/',  # must be an absolute path
    password     => '^{[A-Z0-9-]+}',  # match the scheme identifier
    mail         => undef,
    quota_rule   => undef,
};
my $user_compulsory_fields = [
    'uid', 'gid', 'password'
];

sub fetch_user_data {
    my ($self, $username) = @_;

    my $user_filename = File::Spec->catfile($self->default_values->{user_data_dir}, $username . ".yml");
    return unless ( -e $user_filename );
    die sprintf("Configuration error: wrong permissions for data file (user '%s')", $username) unless ( -r $user_filename );

    my $user_data = eval { LoadFile($user_filename) };
    die sprintf("Configuration error: badly formated data file (user '%s')", $username) if $@ or ref($user_data) ne "HASH";

    # validation
    # (unfortunately no Kwalify module available in EL7)
    foreach my $field (keys %{$user_compulsory_fields}) {
        die sprintf("Configuration error: field '%s' missing in data file (user '%s')") unless defined($user_data->{$field});
    }
    foreach my $field (keys %{$user_struct}) {
	next unless defined($user_data->{$field});
        die sprintf("Configuration error: field '%s' badly formated in data file (user '%s')") unless !defined($user_struct->{$field}) or
            $user_data->{$field} =~ $user_struct->{$field};
    }

    return $user_data;
}

sub process_request {
    my $self   = shift;

    my %L_handler = (
        passdb => sub {
            my ($username) = @_;

	    my $ret = $self->fetch_user_data($username);
	    return unless $ret;

	    # format response
            my $ret2 = {};
            foreach my $field (keys %{$ret}) {
                my $key = ($field eq "password") ? $field : "userdb_" . $field;
		$ret2->{$key} = $ret->{$field};
	    }
            return $ret2;
        },
        userdb => sub {
            my ($username) = @_;

	    my $ret = $self->fetch_user_data($username);
	    delete $ret->{password};
            return $ret;
        },
    );


    # protocol from src/lib-dict/dict-client.h
    my $json = JSON::XS->new;

    eval {
        my $ret;
        # Dict protocol is multiline... go through the lines.
        while (<STDIN>) {
            $self->log(2, "Got request: $_");
            chomp;

            my $cmd = substr($_,0,1);

            next if $cmd eq 'H'; # "hello", skip this line, assume it's ok

            die sprintf("Protocol error: Bad command (%s)", $cmd || '<NONE>') unless ($cmd eq 'L');

            # Process request
            
            my ($namespace,$type,$arg) = split ('/',substr($_,1),3);
            
            if ($namespace eq 'shared') {
                my $f = $L_handler{$type};
                    
                if (defined $f && defined $arg) {
                    $ret = $f->($arg);
                } else {
                    die 'Protocol error: Bad arg';
                }
            } else {
                die sprintf('Protocol error: Bad namespace (%s)', $namespace || '<NONE>');
            }
            last; # Got an "L" , now respond.
        }
        if ($ret) {
            my $json = JSON::XS->new->indent(0)->utf8->encode($ret);
            $self->log(3,"O:$json");
            print "O".$json."\n";
        } else {
            $self->log(3,"NOUSER");
            print "N\n";
        }
        1;
    } or do {
        $self->log(2, "Error: $@");
        print "F\n";
    };
}

sub pre_loop_hook {
    my $self = shift;

    # handle permissions at the containing subdirectory level
    foreach my $sock (@{$self->{'server'}->{'sock'}}) {
        if ($sock->NS_proto eq "UNIX") {
            chmod 0777, $sock->NS_port;
        }
    }

    $self->log(1, 'Starting server');
}

sub pre_server_close_hook {
    my $self = shift;

    $self->log(1, 'Server is shut down');
}

1;

__END__
