---

- name: install Dovecot
  package:
    name:
      - dovecot
      # needed to check version and adapt config
      - openssl
    state: present

- name: "Retrieve Dovecot version #1"
  command: "dovecot --version"
  changed_when: False
  register: _output_dovecot_version
  check_mode: no

- name: "Retrieve Dovecot version #2"
  set_fact:
    _dovecot_version: "{{ _output_dovecot_version['stdout'].split(' ')[0] }}"

- name: "Retrieve Dovecot version #3"
  debug:
    msg: "The detected Dovecot version is: {{ _dovecot_version }}"

- name: "Generate DH file"
  when: _dovecot_version is version('2.3', '>=')
  command: "openssl dhparam -2 -out {{ _dovecot_dh_file }} 2048"
  args:
    creates: "{{ _dovecot_dh_file }}"

- name: "Retrieve openssl version #1"
  shell: |
    set -o pipefail
    openssl version | awk '{ print $2 }'
  args:
    executable: /bin/bash
  changed_when: False
  failed_when: "openssl_version_raw is failed and not (ansible_check_mode and openssl_binary ~ ': No such file or directory' in openssl_version_raw.stderr)"
  register: openssl_version_raw
  check_mode: no

- name: "Retrieve openssl version #2"
  set_fact:
    openssl_version: "{{ openssl_version_raw.stdout }}"

- name: "Retrieve openssl version #3"
  debug:
    var: openssl_version

- name: Clear service ports list
  set_fact:
    _ports: {}
- name: Add IMAP service port
  include_vars: "service_imap.yml"
  when: with_imap
- name: Add IMAPS service port
  include_vars: "service_imaps.yml"
  when: with_imap and with_tls
- name: Add POP3 service port
  include_vars: "service_pop3.yml"
  when: with_pop3
- name: Add POP3S service port
  include_vars: "service_pop3s.yml"
  when: with_pop3 and with_tls

- name: install custom configuration
  template:
    src: templates/dovecot_custom.conf.j2
    dest: /etc/dovecot/conf.d/dovecot_custom.conf
    owner: root
    group: root
    mode: 0644
  notify: restart dovecot

- name: Configure YAML dict authentication method
  include_tasks: yaml-dict-auth.yml
  when: auth == "yaml-dict"

- name: Start service
  service:
    name: dovecot
    enabled: yes
    state: started

- name: Configure firewall
  when: manage_firewall|bool
  include_tasks: firewall.yml

